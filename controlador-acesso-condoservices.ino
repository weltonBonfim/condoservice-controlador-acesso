#include <ESP8266HTTPClient.h>
#include <Wire.h>
#include <MFRC522.h>
#include <SPI.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_Address 0x3C
Adafruit_SSD1306 oled(1);

#define RST_PIN 20  
#define SS_PIN  2  

MFRC522 mfrc522(SS_PIN, RST_PIN);   

//Wireless nome da rede and password
const char* ssid     = "Guilherme"; 
const char* password = "arduino123"; 

void setup() {

  ///Inicializa o Display
  
  oled.begin(SSD1306_SWITCHCAPVCC, OLED_Address);
  
  Serial.begin(9600);    
  SPI.begin();

  ///Inicializa o RFID
            
  mfrc522.PCD_Init();    

  oled.clearDisplay();
  oled.setTextColor(WHITE);
  oled.setCursor(0,0);
  oled.println("Conectando WiFi");
  oled.display();

  /// Inicializa o WIFI
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  
  oled.clearDisplay();
  oled.setTextColor(WHITE);
  oled.setCursor(0,0);
  oled.println("Conectado WiFi");
  oled.display();
  
  delay(3000);
}

void loop() {

  oled.clearDisplay();
  oled.setTextColor(WHITE);
  oled.setCursor(0,0);
  oled.println("Aproxime seu Cartao...");
  oled.display();

  ////Efetua a leitura da TAG ou CARTAO
  
  
  if ( ! mfrc522.PICC_IsNewCardPresent()) {   
    delay(50);
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {   
    delay(50);
    return;
  }

  String uid= "";

  unsigned int hex_num;
  hex_num =  mfrc522.uid.uidByte[0] << 24;
  hex_num += mfrc522.uid.uidByte[1] << 16;
  hex_num += mfrc522.uid.uidByte[2] <<  8;
  hex_num += mfrc522.uid.uidByte[3];

  uid = (String)hex_num;

  mfrc522.PICC_HaltA();

////Conexão com o servidor para validar a entrada...
  
   HTTPClient http;
   http.begin("http://192.168.43.88:8080/rest-api/rest/rfid/validar-entrada/" + uid);
   http.addHeader("Authorization", "1vOW6r4d8M&gOy4$eu@Z4O5!C0v^8M");
   int httpCode = http.GET(); 
   
   if (httpCode > 0) {
    String payload = http.getString();
    
    oled.clearDisplay();
    oled.setTextColor(WHITE);
    oled.setCursor(0,0);
    oled.println(payload);
    oled.display();
    
  }
  
   http.end();

   delay(3000);
}
